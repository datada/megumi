#lang racket

(require web-server/templates)


(let ([title "Templates"]
      [content (xexpr->string 
                `(div ((id "one"))
                      (p ((class "p"))
                         "para")
                      (p ((class "q"))
                         "para2 "
                         "para 2a")))])
  (include-template "_template.html"))

(let ([thing "Templates"])
  (include-template "_simple.html"))
