var NAV = (function (site) {
    return {
        insertNavHTML: function (where, category, current_url) {
            var el = document.getElementById(where);
            if (el) {
                el.innerHTML = this.navHTML(category, current_url);
            }
        },
        navHTML: function (category, current_url) {
            var link, i, s = '';
            for (i = 0; i < site[category].length; i += 1) {
                s += '<li>';
                link = site[category][i];
                s += '<a href="'+link.url+'" title="'+link.title+'">';
                s += link.txt;
                s += '</a>';
            }
            return s;
        }
    };
})({
  undergrad_links: [
    //{url:"Math3EE/index.html", title:"Math3EE" txt:"Course page"},
    {url:"faq.html", title: "FAQ", txt: "Undergraduate FAQ"},
    {url:"letters.html", title: "Letters", txt: "Recommendation Letters FAQ"},						
    {url:"outreach.html", title: "Outreach", txt: "Outreach"}],
  grad_links: [
    {url: 'grad-faq.html', title: 'GradFAQ', txt:'Graduate FAQ'}, 
    {url: 'advisee.html', title: 'AdviseeFAQ', txt:'Potential advisee FAQ'}],
  research_links: [	
    {url:"publications.html", title: "See my publications.", txt: "Publications"},
		{url: "activities.html", title: "Orgainational activities.", txt: "Math Activities"},
		{url:"resources.html", title: "See related notes and resources.", txt: "Notes and Resources"}],
  etc:[]});

